﻿using System.Runtime.Serialization;

namespace Stracon.Catip.Domain.Enums
{
    public enum MaterialResponseCode
    {
        [EnumMember(Value = "ItemAccepted")]
        ItemAccepted,
        [EnumMember(Value = "ItemRejected")]
        ItemRejected,
        [EnumMember(Value = "IndirectlyReplaced")]
        IndirectlyReplaced,
        [EnumMember(Value = "SingleReplacement")]
        SingleReplacement,
        [EnumMember(Value = "MultiReplacement")]
        MultiReplacement,
        [EnumMember(Value = "ReplacementItem")]
        ReplacementItem,
        [EnumMember(Value = "Other")]
        Other
    }
}
