﻿using Stracon.Catip.Domain.Common;

namespace Stracon.Catip.Domain.Entities
{
    public class ConfigEntity : AuditableEntity
    {
        public int ConfigId { get; set; }
        public string ConfigKey { get; set; }
        public string ConfigVal { get; set; }
    }
}
