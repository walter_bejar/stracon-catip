﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace Stracon.Catip.WebApi.Helpers
{
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        public BasicAuthenticationHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Authorization"))
                return Task.FromResult(AuthenticateResult.Fail("Header de autorización no encontrado"));

            try
            {
                var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
                var credentials = Encoding.UTF8.GetString(credentialBytes).Split(new char[] { ':' }, 2);
                var username = credentials[0];
                var password = credentials[1];

                //if (!username.Equals("S4STRACON") || !password.Equals("Stracon21@"))
                //    return Task.FromResult(AuthenticateResult.Fail("Usuario o password inválido"));
                if (!username.Equals("S4MINSUR") || !password.Equals("Minsur21@"))
                    return Task.FromResult(AuthenticateResult.Fail("Usuario o password inválido"));
            }
            catch
            {
                return Task.FromResult(AuthenticateResult.Fail("Header de autorización inválido"));
            }

            var claims = new[] {
                new Claim(ClaimTypes.NameIdentifier, "01"),
                new Claim(ClaimTypes.Name, "S4STRACON"),
            };
            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);

            return Task.FromResult(AuthenticateResult.Success(ticket));
        }
    }
}
