﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Stracon.Catip.Application.PriceAvailability.Query.GetAvailability;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Stracon.Catip.WebApi.Controllers
{
    [Authorize]
    [Route("api/v1/price-availability")]
    public class PriceAvailabilityController : ApiController
    {
        [HttpPost]
        public async Task<ActionResult<List<MaterialAvailabilityModel>>> PriceAvailabilityRequest(string id, List<MaterialModel> materials)
        {
            return await Mediator.Send(new GetAvailabilityQuery
            {
                Id = "-",
                Materials = materials
            });
        }        
    }
}
