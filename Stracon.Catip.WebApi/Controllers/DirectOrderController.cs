﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Stracon.Catip.Application.DirectOrder.Commands.PostOrder;
using System.Threading.Tasks;

using PlacedOrderModel = Stracon.Catip.Application.DirectOrder.Commands.PostOrder.OrderModel;

namespace Stracon.Catip.WebApi.Controllers
{
    [Authorize]
    [Route("api/v1/direct-order")]
    public class DirectOrderController : ApiController
    {
        [HttpPost("request")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<ActionResult> ProcessRequest(PlacedOrderModel materialsOrder)
        {
            await Mediator.Send(new PostOrderCommand { Order = materialsOrder });
            return NoContent();
        }

        [HttpPost("response")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<ActionResult> ProcessResponsePost()
        {
            return Ok();
        }

        [HttpGet("response")]
        [Produces("application/json")]
        public async Task<ActionResult> ProcessResponseGet()
        {
            return Ok();
        }
    }
}
