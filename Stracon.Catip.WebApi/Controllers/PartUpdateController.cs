﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stracon.Catip.WebApi.Controllers
{
    [Authorize]
    [Route("api/v1/part-update")]
    public class PartUpdateController : ApiController
    {
        [HttpPost("response")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<ActionResult> ProcessResponsePost()
        {
            return Ok();
        }

        [HttpGet("response")]
        [Produces("application/json")]
        public async Task<ActionResult> ProcessResponseGet()
        {
            return Ok();
        }
    }
}
