﻿using System.Collections.Generic;

namespace Stracon.Catip.Application.DirectOrder.Commands.PostOrder
{
    public class OrderModel
    {
        public string OrderNumber { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public List<MaterialModel> Materials { get; set; }
    }
}
