﻿using MediatR;
using Stracon.Catip.Application.Common.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace Stracon.Catip.Application.DirectOrder.Commands.PostOrder
{
    public class PostOrderCommand : IRequest
    {
        public OrderModel Order { get; set; }
    }

    public class PostOrderCommandHandler : IRequestHandler<PostOrderCommand>
    {
        private readonly IDirectOrderService _requestService;

        public PostOrderCommandHandler(IDirectOrderService requestService)
        {
            _requestService = requestService;
        }

        public async Task<Unit> Handle(PostOrderCommand request, CancellationToken cancellationToken)
        {
            await _requestService.PlaceOrderAsync(request.Order);
            return Unit.Value;
        }
    }
}
