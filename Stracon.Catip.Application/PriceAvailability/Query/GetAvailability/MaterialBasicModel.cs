﻿using System.Collections.Generic;

namespace Stracon.Catip.Application.PriceAvailability.Query.GetAvailability
{
    public class MaterialBasicModel
    {
        public string VendorMaterialNo { get; set; }
        public string VendorMaterialSos { get; set; }
        public int TotalQuantity { get; set; }
        public int BackOrderQuantity { get; set; }
        public int OnSiteQuantity { get; set; }
        public decimal MaterialPrice { get; set; }
        public bool IsAvailable { get; set; } = false;
        public List<StoreAvailabilityModel> StoreAvailabilities { get; set; } = new List<StoreAvailabilityModel>();
    }
}
