﻿using System.Numerics;

namespace Stracon.Catip.Application.PriceAvailability.Query.GetAvailability
{
    public class MaterialModel
    {
        public int ItemPositionNo { get; set; }
        public string MaterialNo { get; set; }
        public string VendorMaterialNo { get; set; }
        public string VendorMaterialSos { get; set; }
        public decimal Quantity { get; set; }
    }
}
