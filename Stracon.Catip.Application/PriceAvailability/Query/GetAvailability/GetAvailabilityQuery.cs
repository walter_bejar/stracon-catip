﻿using MediatR;
using Stracon.Catip.Application.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Stracon.Catip.Application.PriceAvailability.Query.GetAvailability
{
    public class GetAvailabilityQuery : IRequest<List<MaterialAvailabilityModel>>
    {
        public string Id { get; set; }
        public List<MaterialModel> Materials { get; set; }
    }

    public class GetAvailabilityQueryHandler : IRequestHandler<GetAvailabilityQuery, List<MaterialAvailabilityModel>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IPriceAvailabilityService _availabilityService;

        public GetAvailabilityQueryHandler(IApplicationDbContext context, IPriceAvailabilityService availabilityService)
        {
            _context = context;
            _availabilityService = availabilityService;
        }

        public async Task<List<MaterialAvailabilityModel>> Handle(GetAvailabilityQuery request, CancellationToken cancellationToken)
        {

            return await _availabilityService.CheckMaterialsAvailabilityAsync(request.Materials);
        }
    }
}
