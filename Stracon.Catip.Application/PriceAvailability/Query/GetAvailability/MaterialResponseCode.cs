﻿using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Stracon.Catip.Application.PriceAvailability.Query.GetAvailability
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum MaterialResponseCode
    {
        [EnumMember(Value = "ItemAccepted")]
        ItemAccepted,
        [EnumMember(Value = "ItemRejected")]
        ItemRejected,
        [EnumMember(Value = "IndirectlyReplaced")]
        IndirectlyReplaced,
        [EnumMember(Value = "SingleReplacement")]
        SingleReplacement,
        [EnumMember(Value = "MultiReplacement")]
        MultiReplacement
    }
}
