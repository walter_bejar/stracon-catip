﻿namespace Stracon.Catip.Application.PriceAvailability.Query.GetAvailability
{
    public class StoreAvailabilityModel
    {
        public string StoreName { get; set; }
        public string StoreQuantity { get; set; }
    }
}
