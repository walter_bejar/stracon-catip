﻿using System.Collections.Generic;

namespace Stracon.Catip.Application.PriceAvailability.Query.GetAvailability
{
    public class MaterialAvailabilityModel
    {
        public MaterialResponseCode MaterialResponseCode { get; set; }
        public string MaterialResponseText { get; set; } = "";
        public int ItemPositionNo { get; set; }
        public string MaterialNo { get; set; }
        public string VendorMaterialNo { get; set; }
        public string VendorMaterialSos { get; set; }
        public decimal Quantity { get; set; }
        public decimal QuantityBackOrder { get; set; }
        public decimal QuantityOnSite { get; set; }
        public decimal MaterialPrice { get; set; }
        public decimal DiscountPrice { get; set; }
        public bool IsAvailable { get; set; } = false;
        public List<StoreAvailabilityModel> Stores { get; set; } = new List<StoreAvailabilityModel>();
        public bool HasReplacement { get; set; } = false;
        public List<MaterialBasicModel> MaterialReplacements { get; set; } = new List<MaterialBasicModel>();
        public bool HasAlternateItems { get; set; } = false;
        public List<MaterialBasicModel> AlternateItems { get; set; } = new List<MaterialBasicModel>();
    }
}
