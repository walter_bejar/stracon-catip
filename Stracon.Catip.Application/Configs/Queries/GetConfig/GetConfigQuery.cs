﻿using AutoMapper;
using MediatR;
using Stracon.Catip.Application.Common.Exceptions;
using Stracon.Catip.Application.Common.Interfaces;
using Stracon.Catip.Domain.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Stracon.Catip.Application.Configs.Queries.GetConfig
{
    public class GetConfigQuery : IRequest<ConfigModel>
    {
        public string ConfigKey { get; set; }
    }

    public class GetConfigQueryHandler : IRequestHandler<GetConfigQuery, ConfigModel>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetConfigQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ConfigModel> Handle(GetConfigQuery request, CancellationToken cancellationToken)
        {
            ConfigEntity config = _context.Configs.Where(c => c.ConfigKey.Equals(request.ConfigKey)).FirstOrDefault();

            if (config == null)
                throw new NotFoundException("config", request.ConfigKey);

            ConfigModel configModel = _mapper.Map<ConfigModel>(config);
            return await Task.FromResult(configModel);
        }
    }
}
