﻿using Stracon.Catip.Application.Common.Mappings;
using Stracon.Catip.Domain.Entities;

namespace Stracon.Catip.Application.Configs.Queries.GetConfig
{
    public class ConfigModel : IMapFrom<ConfigEntity>
    {
        public string ConfigKey { get; set; }
        public string ConfigVal { get; set; }
    }
}
