﻿using System;

namespace Stracon.Catip.Application.Common.Exceptions
{
    public class CatipException : Exception
    {
        public CatipException()
            : base()
        {
        }

        public CatipException(string message)
            : base(message)
        {
        }

        public CatipException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
