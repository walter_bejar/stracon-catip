﻿using Stracon.Catip.Application.DirectOrder.Commands.PostOrder;
using System.Threading.Tasks;

namespace Stracon.Catip.Application.Common.Interfaces
{
    public interface IDirectOrderService
    {
        Task PlaceOrderAsync(OrderModel materialsOrder);
    }
}
