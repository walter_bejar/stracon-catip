﻿using Microsoft.EntityFrameworkCore;
using Stracon.Catip.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Stracon.Catip.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        public DbSet<ConfigEntity> Configs { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
