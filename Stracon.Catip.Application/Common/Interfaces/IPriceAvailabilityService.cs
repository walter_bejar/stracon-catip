﻿using Stracon.Catip.Application.PriceAvailability.Query.GetAvailability;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Stracon.Catip.Application.Common.Interfaces
{
    public interface IPriceAvailabilityService
    {
        Task<List<MaterialAvailabilityModel>> CheckMaterialsAvailabilityAsync(List<MaterialModel> materials);
    }
}
