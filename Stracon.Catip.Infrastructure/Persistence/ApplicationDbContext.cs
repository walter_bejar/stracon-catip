﻿using Microsoft.EntityFrameworkCore;
using Stracon.Catip.Application.Common.Interfaces;
using Stracon.Catip.Domain.Entities;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Stracon.Catip.Infrastructure.Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public DbSet<ConfigEntity> Configs { get; set; }

        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }        

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            return base.SaveChangesAsync(cancellationToken);
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(@"Server=localhost;Initial Catalog=StraconCatipDb;User Id=sa;Password=Tintaya123.;");
        //}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(builder);
        }
    }
}
