﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Stracon.Catip.Domain.Entities;

namespace Stracon.Catip.Infrastructure.Persistence.Configurations
{
    public class ConfigEntityConfiguration : IEntityTypeConfiguration<ConfigEntity>
    {
        public void Configure(EntityTypeBuilder<ConfigEntity> builder)
        {
            builder.HasKey(k => k.ConfigId);

            builder.Property(p => p.ConfigId)
                .ValueGeneratedOnAdd();

            builder.Property(p => p.ConfigKey)
                .HasMaxLength(30);

            builder.Property(p => p.ConfigVal)
                .HasMaxLength(200);
        }
    }
}
