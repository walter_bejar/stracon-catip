﻿using System.Xml.Serialization;

namespace Stracon.Catip.Infrastructure.Models.OrderAcknowledgement
{
    public partial class OrderAcknowledgement
    {
        private string errorCode;
        private string errorMessage;
        private string detailMessage;
        private string timeStamp;
        private string status;

        public string ErrorCode { get => errorCode; set => errorCode = value; }
        public string ErrorMessage { get => errorMessage; set => errorMessage = value; }
        public string DetailMessage { get => detailMessage; set => detailMessage = value; }

        [XmlAttribute]
        public string TimeStamp { get => timeStamp; set => timeStamp = value; }

        [XmlAttribute]
        public string Status { get => status; set => status = value; }
    }
}
