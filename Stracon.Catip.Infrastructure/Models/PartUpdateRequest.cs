﻿namespace Stracon.Catip.Infrastructure.Models.PartUpdate.Request {
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=false)]
    public partial class PartUpdate {
        
        private PartUpdateRequest requestField;
        
        /// <remarks/>
        public PartUpdateRequest Request {
            get {
                return this.requestField;
            }
            set {
                this.requestField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class PartUpdateRequest {
        
        private string customerIDField;
        
        private string supplierIDField;
        
        private string correlationIDField;
        
        private string outLineAgreementField;
        
        private string centralContractField;
        
        private string locationField;
        
        private string accountField;
        
        private PartUpdateRequestRequestPart[] partsListField;
        
        /// <remarks/>
        public string CustomerID {
            get {
                return this.customerIDField;
            }
            set {
                this.customerIDField = value;
            }
        }
        
        /// <remarks/>
        public string SupplierID {
            get {
                return this.supplierIDField;
            }
            set {
                this.supplierIDField = value;
            }
        }
        
        /// <remarks/>
        public string CorrelationID {
            get {
                return this.correlationIDField;
            }
            set {
                this.correlationIDField = value;
            }
        }
        
        /// <remarks/>
        public string OutLineAgreement {
            get {
                return this.outLineAgreementField;
            }
            set {
                this.outLineAgreementField = value;
            }
        }
        
        /// <remarks/>
        public string CentralContract {
            get {
                return this.centralContractField;
            }
            set {
                this.centralContractField = value;
            }
        }
        
        /// <remarks/>
        public string Location {
            get {
                return this.locationField;
            }
            set {
                this.locationField = value;
            }
        }
        
        /// <remarks/>
        public string Account {
            get {
                return this.accountField;
            }
            set {
                this.accountField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("RequestPart", IsNullable=false)]
        public PartUpdateRequestRequestPart[] PartsList {
            get {
                return this.partsListField;
            }
            set {
                this.partsListField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class PartUpdateRequestRequestPart {
        
        private uint customerLineNumberField;
        
        private string customerPartNumberField;
        
        private string manufacturerPartNumberField;
        
        private string manufacturerCodeField;
        
        private string manufacturerCodeDescripField;
        
        private string partDescriptionField;
        
        private decimal unitPriceField;
        
        private decimal coreUnitPriceField;
        
        private bool coreUnitPriceFieldSpecified;
        
        private string currencyField;
        
        private string unitOfMeasureField;
        
        /// <remarks/>
        public uint CustomerLineNumber {
            get {
                return this.customerLineNumberField;
            }
            set {
                this.customerLineNumberField = value;
            }
        }
        
        /// <remarks/>
        public string CustomerPartNumber {
            get {
                return this.customerPartNumberField;
            }
            set {
                this.customerPartNumberField = value;
            }
        }
        
        /// <remarks/>
        public string ManufacturerPartNumber {
            get {
                return this.manufacturerPartNumberField;
            }
            set {
                this.manufacturerPartNumberField = value;
            }
        }
        
        /// <remarks/>
        public string ManufacturerCode {
            get {
                return this.manufacturerCodeField;
            }
            set {
                this.manufacturerCodeField = value;
            }
        }
        
        /// <remarks/>
        public string ManufacturerCodeDescrip {
            get {
                return this.manufacturerCodeDescripField;
            }
            set {
                this.manufacturerCodeDescripField = value;
            }
        }
        
        /// <remarks/>
        public string PartDescription {
            get {
                return this.partDescriptionField;
            }
            set {
                this.partDescriptionField = value;
            }
        }
        
        /// <remarks/>
        public decimal UnitPrice {
            get {
                return this.unitPriceField;
            }
            set {
                this.unitPriceField = value;
            }
        }
        
        /// <remarks/>
        public decimal CoreUnitPrice {
            get {
                return this.coreUnitPriceField;
            }
            set {
                this.coreUnitPriceField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CoreUnitPriceSpecified {
            get {
                return this.coreUnitPriceFieldSpecified;
            }
            set {
                this.coreUnitPriceFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public string Currency {
            get {
                return this.currencyField;
            }
            set {
                this.currencyField = value;
            }
        }
        
        /// <remarks/>
        public string UnitOfMeasure {
            get {
                return this.unitOfMeasureField;
            }
            set {
                this.unitOfMeasureField = value;
            }
        }
    }
}
