﻿using MediatR;
using Stracon.Catip.Application.DirectOrder.Commands.PostOrder;
using Stracon.Catip.Infrastructure.Models.Order;
using System;
using System.Collections.Generic;

namespace Stracon.Catip.Infrastructure.Utils.Request
{
    public static class DirectOrderRequest
    {
        public static Order CreateRequest(OrderModel orderModel, Dictionary<string, string> configs, IMediator _mediator)
        {
            Order order = new Order();
            OrderHeader orderHeader = new OrderHeader();
            orderHeader.OrderIssueDate = DateTime.Now.ToString("yyyyMMdd'T'HH:mm:ss");
            order.OrderHeader = orderHeader;

            OrderHeaderOrderCurrency orderHeaderOrderCurrency = new OrderHeaderOrderCurrency();
            Currency currency = new Currency();
            currency.CurrencyCoded = CurrencyCode.USD;
            orderHeaderOrderCurrency.Currency = currency;
            orderHeader.OrderCurrency = orderHeaderOrderCurrency;

            OrderHeaderOrderLanguage orderHeaderOrderLanguage = new OrderHeaderOrderLanguage();
            Language language = new Language();
            language.LanguageCoded = LanguageCode.en;
            orderHeaderOrderLanguage.Language = language;
            orderHeader.OrderLanguage = orderHeaderOrderLanguage;

            // Para setear el número de orden
            OrderNumber orderNumber = new OrderNumber();
            orderNumber.BuyerOrderNumber = orderModel.OrderNumber;
            orderHeader.OrderNumber = orderNumber;

            // Para setear el customer account
            OrderReferences orderReferences = new OrderReferences();
            OrderReferencesAccountCode orderReferencesAccountCode = new OrderReferencesAccountCode();
            Reference reference = new Reference();
            reference.RefNum = configs["account"]; //"1054306";//"0903167";
            orderReferencesAccountCode.Reference = reference;
            orderReferences.AccountCode = orderReferencesAccountCode;
            orderHeader.OrderReferences = orderReferences;

            OrderReferencesOtherOrderReferences orderReferencesOtherOrderReferences = new OrderReferencesOtherOrderReferences();
            ListOfReferenceCoded listOfReferenceCoded = new ListOfReferenceCoded();
            listOfReferenceCoded.ReferenceCoded = new ReferenceCoded[3];
            orderReferencesOtherOrderReferences.ListOfReferenceCoded = listOfReferenceCoded.ReferenceCoded;
            orderReferences.OtherOrderReferences = orderReferencesOtherOrderReferences;

            // Para setear el transaction id
            ReferenceCoded referenceCodedTxId = new ReferenceCoded();
            referenceCodedTxId.ReferenceTypeCoded = ReferenceTypeCode.SendersReferenceToTheOriginalMessage;
            ReferenceCodedPrimaryReference referenceCodedPrimaryReferenceTxId = new ReferenceCodedPrimaryReference();
            Reference referenceTxId = new Reference();
            referenceTxId.RefNum = DateTime.Now.ToString("yyyyMMddHHmmss");//"TXH101";
            referenceCodedPrimaryReferenceTxId.Reference = referenceTxId;
            referenceCodedTxId.PrimaryReference = referenceCodedPrimaryReferenceTxId;
            listOfReferenceCoded.ReferenceCoded[0] = referenceCodedTxId;

            // Para setear el dealer code
            ReferenceCoded referenceCodedDealerCode = new ReferenceCoded();
            referenceCodedDealerCode.ReferenceTypeCoded = ReferenceTypeCode.DealerNumber;
            ReferenceCodedPrimaryReference referenceCodedPrimaryReferenceDealerCode = new ReferenceCodedPrimaryReference();
            Reference referenceDealerCode = new Reference();
            referenceDealerCode.RefNum = configs["dealer_code"]; //"R080";//"R081";
            referenceCodedPrimaryReferenceDealerCode.Reference = referenceDealerCode;
            referenceCodedDealerCode.PrimaryReference = referenceCodedPrimaryReferenceDealerCode;
            listOfReferenceCoded.ReferenceCoded[1] = referenceCodedDealerCode;

            // Para setear end use code
            ReferenceCoded referenceCodedEndCode = new ReferenceCoded();
            referenceCodedEndCode.ReferenceTypeCoded = ReferenceTypeCode.EndUseNumber;
            ReferenceCodedPrimaryReference referenceCodedPrimaryReferenceEndCode = new ReferenceCodedPrimaryReference();
            Reference referenceEndCode = new Reference();
            referenceEndCode.RefNum = configs["refnum"];//"2";
            referenceCodedPrimaryReferenceEndCode.Reference = referenceEndCode;
            referenceCodedEndCode.PrimaryReference = referenceCodedPrimaryReferenceEndCode;
            listOfReferenceCoded.ReferenceCoded[2] = referenceCodedEndCode;

            // Para setear purpose
            Purpose purpose = new Purpose();
            purpose.PurposeCoded = PurposeCode.Original;
            orderHeader.Purpose = purpose;

            // Para setear el order type
            OrderType orderType = new OrderType();
            orderType.OrderTypeCoded = OrderTypeCode.Order;
            orderHeader.OrderType = orderType;

            OrderParty orderParty = new OrderParty();
            orderHeader.OrderParty = orderParty;

            // Para setear el customer tpid
            OrderPartyBuyerParty orderPartyBuyerParty = new OrderPartyBuyerParty();
            Party partyCustomerId = new Party();
            PartyPartyID partyPartyIDCustomerId = new PartyPartyID();
            Identifier identifierCustomerId = new Identifier();
            identifierCustomerId.Ident = "";
            Agency agencyCustomerId = new Agency();
            agencyCustomerId.AgencyCoded = AgencyCode.AssignedBySellerOrSellersAgent;
            agencyCustomerId.CodeListIdentifierCodedSpecified = true;
            agencyCustomerId.CodeListIdentifierCoded = CodeListIdentifierCode.Other;
            agencyCustomerId.CodeListIdentifierCodedOther = configs["customer_id"]; //"HUDBAY";//"903167";
            identifierCustomerId.Agency = agencyCustomerId;
            partyPartyIDCustomerId.Identifier = identifierCustomerId;
            partyCustomerId.PartyID = partyPartyIDCustomerId;
            orderPartyBuyerParty.Party = partyCustomerId;
            orderParty.BuyerParty = orderPartyBuyerParty;

            // Para setear el contacto
            PartyOrderContact partyOrderContact = new PartyOrderContact();
            Contact contact = new Contact();
            contact.ContactName = orderModel.ContactName;

            ContactNumber contactPhone = new ContactNumber();
            contactPhone.ContactNumberTypeCoded = ContactNumberTypeCode.TelephoneNumber;
            contactPhone.ContactNumberValue = orderModel.ContactPhone;

            ContactNumber contactEmail = new ContactNumber();
            contactEmail.ContactNumberTypeCoded = ContactNumberTypeCode.EmailAddress;
            contactEmail.ContactNumberValue = orderModel.ContactEmail;

            List<ContactNumber> contactNumbers = new List<ContactNumber> { contactPhone, contactEmail };
            contact.ListOfContactNumber = contactNumbers.ToArray();
            partyOrderContact.Contact = contact;
            partyCustomerId.OrderContact = partyOrderContact;


            // Para setear el supplier tpid
            OrderPartySellerParty orderPartySellerParty = new OrderPartySellerParty();
            Party partySupplierId = new Party();
            PartyPartyID partyPartyIDSupplierId = new PartyPartyID();
            Identifier identifierSupplierId = new Identifier();
            identifierSupplierId.Ident = "";
            Agency agencySupplierId = new Agency();
            agencySupplierId.AgencyCoded = AgencyCode.AssignedByBuyerOrBuyersAgent;
            agencySupplierId.CodeListIdentifierCodedSpecified = true;
            agencySupplierId.CodeListIdentifierCoded = CodeListIdentifierCode.Other;
            agencySupplierId.CodeListIdentifierCodedOther = configs["supplier_id"]; //"R080"; //"R081";
            identifierSupplierId.Agency = agencySupplierId;
            partyPartyIDSupplierId.Identifier = identifierSupplierId;
            partySupplierId.PartyID = partyPartyIDSupplierId;
            orderPartySellerParty.Party = partySupplierId;
            orderParty.SellerParty = orderPartySellerParty;

            // Para setear part location
            orderHeader.PartLocation = configs["store"]; //"2081"; //"2002"; //"2081"; //"83";//"60";

            // Para setear el tipo de entrega
            ListOfTransport listOfTransport = new ListOfTransport();
            listOfTransport.Transport = new Transport[1];
            Transport transport = new Transport();
            transport.TransportID = Convert.ToInt32(configs["transport_id"]); //1;
            transport.ShippingInstructions = configs["shipping_instructions"]; //"01";
            TransportMode transportMode = new TransportMode();
            // Tipo de transporte
            transportMode.TransportModeCoded = TransportModeCode.CustomerPickup; //TransportModeCode.Delivery
            transportMode.TransportModeCodedOther = "";
            transport.TransportMode = transportMode;
            listOfTransport.Transport[0] = transport;
            orderHeader.ListOfTransport = listOfTransport.Transport;

            // Para crear el detalle de materiales
            OrderDetail orderDetail = new OrderDetail();
            List<ItemDetail> itemDetails = new List<ItemDetail>();

            foreach (MaterialModel material in orderModel.Materials)
            {
                ItemDetail itemDetail = new ItemDetail();
                BaseItemDetail baseItemDetail = new BaseItemDetail();
                PricingDetail pricingDetail = new PricingDetail();
                itemDetail.BaseItemDetail = baseItemDetail;
                itemDetail.PricingDetail = pricingDetail;

                // Para setear el número de item
                LineItemNum lineItemNum = new LineItemNum();
                lineItemNum.BuyerLineItemNum = material.ItemPositionNo;
                baseItemDetail.LineItemNum = lineItemNum;

                // Para setear la cantidad
                BaseItemDetailTotalQuantity baseItemDetailTotalQuantity = new BaseItemDetailTotalQuantity();
                baseItemDetail.TotalQuantity = baseItemDetailTotalQuantity;
                Quantity quantity = new Quantity();
                baseItemDetailTotalQuantity.Quantity = quantity;
                QuantityValue quantityValue = new QuantityValue();
                quantityValue.Value = material.Quantity;
                quantity.QuantityValue = quantityValue;
                UnitOfMeasurement unitOfMeasurement = new UnitOfMeasurement();
                unitOfMeasurement.UOMCoded = UOMCode.EA;
                quantity.UnitOfMeasurement = unitOfMeasurement;

                // Para setear número de material, número de parte y sos
                ItemIdentifiers itemIdentifiers = new ItemIdentifiers();
                baseItemDetail.ItemIdentifiers = itemIdentifiers;
                PartNumbers partNumbers = new PartNumbers();
                itemIdentifiers.PartNumbers = partNumbers;
                PartNumbersBuyerPartNumber partNumbersBuyerPartNumber = new PartNumbersBuyerPartNumber();
                PartNum partNum = new PartNum();
                partNum.PartID = material.MaterialNo;
                partNumbersBuyerPartNumber.PartNum = partNum;
                partNumbers.BuyerPartNumber = partNumbersBuyerPartNumber;

                ManufacturerPartNumber manufacturerPartNumber = new ManufacturerPartNumber();
                manufacturerPartNumber.PartID = material.VendorMaterialNo; // se utiliza el número de parte alternativo
                manufacturerPartNumber.PartIDExt = material.VendorMaterialSos;
                partNumbers.ManufacturerPartNumber = manufacturerPartNumber;

                // Para agregar el número de serie
                //PartNumbersOtherItemIdentifiers partNumbersOtherItemIdentifiers = new PartNumbersOtherItemIdentifiers();
                //partNumbersOtherItemIdentifiers.ListOfProductIdentifierCoded = new ProductIdentifierCoded[1];
                //ProductIdentifierCoded productIdentifierCoded = new ProductIdentifierCoded();
                //productIdentifierCoded.ProductIdentifierQualifierCoded = ProductIdentifierQualifierCode.SerialNumber;
                //productIdentifierCoded.ProductIdentifierQualifierCodedOther = orderModel.SerialNumber;
                //productIdentifierCoded.ProductIdentifier = "";
                //partNumbersOtherItemIdentifiers.ListOfProductIdentifierCoded[0] = productIdentifierCoded;
                //partNumbers.OtherItemIdentifiers = partNumbersOtherItemIdentifiers;

                // Para agregar el request by date
                //DeliveryDetail deliveryDetail = new DeliveryDetail();
                //deliveryDetail.ListOfScheduleLine = new ScheduleLine[1];
                //ScheduleLine scheduleLine = new ScheduleLine();

                //Quantity quantityDate = new Quantity();
                //QuantityValue quantityValueDate = new QuantityValue();
                //quantityValueDate.Value = material.Quantity;
                //quantityDate.QuantityValue = quantityValueDate;

                //UnitOfMeasurement unitOfMeasurementDate = new UnitOfMeasurement();
                //unitOfMeasurementDate.UOMCoded = UOMCode.EA;
                //quantityDate.UnitOfMeasurement = unitOfMeasurementDate;

                //scheduleLine.Quantity = quantityDate;

                //string dateRequest = orderModel.RequestDeliveryDate;

                //try
                //{
                //    CultureInfo provider = CultureInfo.InvariantCulture;
                //    DateTime date = DateTime.ParseExact(orderModel.RequestDeliveryDate, "yyyyMMdd", provider);
                //    dateRequest = date.ToString("dd.MM.yyyy");
                //}
                //catch (Exception e)
                //{
                //    dateRequest = orderModel.RequestDeliveryDate;
                //}

                //scheduleLine.RequestedDeliveryDate = dateRequest;
                //deliveryDetail.ListOfScheduleLine[0] = scheduleLine;
                //itemDetail.DeliveryDetail = deliveryDetail;

                // Para setear el precio
                Price price = new Price();
                UnitPrice unitPrice = new UnitPrice();
                unitPrice.UnitPriceValue = material.MaterialPrice;
                price.UnitPrice = unitPrice;
                pricingDetail.ListOfPrice = new List<Price>() { price }.ToArray();

                itemDetails.Add(itemDetail);
            }

            orderDetail.ListOfItemDetail = itemDetails.ToArray();
            order.OrderDetail = orderDetail;

            return order;
        }
    }
}
