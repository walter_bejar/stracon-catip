﻿using MediatR;
using Stracon.Catip.Application.PriceAvailability.Query.GetAvailability;
using Stracon.Catip.Infrastructure.Models.OrderResponse;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Stracon.Catip.Infrastructure.Utils.Response
{
    public static class PriceAvailabilityResponse
    {
        public static List<MaterialAvailabilityModel> ReadResponse(OrderResponse orderResponse, IMediator _mediator)
        {
            Dictionary<int, MaterialAvailabilityModel> keyValuePairs = new Dictionary<int, MaterialAvailabilityModel>();

            // Validamos si la respuesta es válida
            OrderResponseHeader orderResponseHeader = orderResponse.OrderResponseHeader;
            ResponseType responseType = orderResponseHeader.ResponseType;
            ResponseTypeCode responseTypeCode = responseType.ResponseTypeCoded;

            if (responseTypeCode != ResponseTypeCode.ConditionallyAccepted)
            {
                OrderResponseSummary orderResponseSummary = orderResponse.OrderResponseSummary;
                ErrorInfo errorInfo = orderResponseSummary.ErrorInfo;
                string message = errorInfo.CompletionMsg.LanguageString.LangString;

                throw new Exception(message);
            }

            // Extraemos los items de respuesta
            OrderResponseDetail orderResponseDetail = orderResponse.OrderResponseDetail;
            OrderResponseItemDetail[] listOfOrderResponseItemDetail = orderResponseDetail.ListOfOrderResponseItemDetail;

            foreach (OrderResponseItemDetail orderResponseItemDetail in listOfOrderResponseItemDetail)
            {
                DetailResponseCode detailResponseCode = orderResponseItemDetail.ItemDetailResponseCoded;
                MaterialAvailabilityModel materialDetail = new MaterialAvailabilityModel();

                // Verificamos los estados
                if (detailResponseCode == DetailResponseCode.ItemAccepted)
                {
                    ItemDetailChanges itemDetailChanges = orderResponseItemDetail.ItemDetailChanges;
                    ItemDetail itemDetail = itemDetailChanges.ItemDetail;
                    BaseItemDetail baseItemDetail = itemDetail.BaseItemDetail;
                    ItemIdentifiers itemIdentifiers = baseItemDetail.ItemIdentifiers;
                    ManufacturerPartNumber manufacturerPartNumber = itemIdentifiers.PartNumbers.ManufacturerPartNumber;
                    PricingDetail pricingDetail = itemDetail.PricingDetail;

                    OriginalItemDetail originalItemDetail = orderResponseItemDetail.OriginalItemDetail;
                    PartNumbers partNumbers = originalItemDetail.ItemDetail.BaseItemDetail.ItemIdentifiers.PartNumbers;

                    materialDetail.MaterialNo = partNumbers.BuyerPartNumber.PartNum.PartID;
                    materialDetail.ItemPositionNo = baseItemDetail.LineItemNum.BuyerLineItemNum;
                    materialDetail.VendorMaterialNo = manufacturerPartNumber.PartID;
                    materialDetail.VendorMaterialSos = manufacturerPartNumber.PartIDExt;
                    materialDetail.IsAvailable = true;
                    materialDetail.HasReplacement = false;
                    materialDetail.MaterialResponseCode = MaterialResponseCode.ItemAccepted;

                    BaseItemDetailMaxBackOrderQuantity baseItemDetailMaxBackOrderQuantity = baseItemDetail.MaxBackOrderQuantity;
                    decimal backOrderQuantity = baseItemDetailMaxBackOrderQuantity.Quantity.QuantityValue.Value;

                    BaseItemDetailTotalQuantity baseItemDetailTotalQuantity = baseItemDetail.TotalQuantity;
                    decimal totalOrderQuantity = baseItemDetailTotalQuantity.Quantity.QuantityValue.Value;
                    materialDetail.Quantity = totalOrderQuantity;
                    materialDetail.QuantityBackOrder = backOrderQuantity;
                    //SMaterialModel material = _mediator.Send(new GetMaterialQuery
                    //{
                    //    VendorMaterialNo = materialDetail.VendorMaterialNo,
                    //    VendorMaterialSos = materialDetail.VendorMaterialSos
                    //}).Result;

                    materialDetail.QuantityOnSite = totalOrderQuantity - backOrderQuantity;

                    /*materialDetail.OnSiteQuantity = materialDetail.TotalQuantity - materialDetail.BackOrderQuantity;*/
                    //materialDetail.Stores.Add(
                    //    new StoreAvailabilityModel
                    //    {
                    //        StoreName = "2081 Stracon On-Site",
                    //        StoreQuantity = materialDetail.QuantityOnSite.ToString()
                    //    });

                    if (backOrderQuantity > 0)
                    {
                        NameValueSet[] listOfNameValueSet = itemDetail.ListOfNameValueSet;

                        foreach (NameValueSet nameValueSet in listOfNameValueSet)
                        {
                            NameValuePair[] listOfNameValuePair = nameValueSet.ListOfNameValuePair;

                            string facilityCode = listOfNameValuePair[0].Value;
                            string facilityName = listOfNameValuePair[1].Value;
                            string facilityQty = listOfNameValuePair[2].Value;

                            if (facilityName == null || facilityName == "")
                                facilityName = "-";

                            if (facilityCode == null || facilityCode == "")
                                facilityCode = "-";

                            materialDetail.Stores.Add(
                                new StoreAvailabilityModel
                                {
                                    StoreName = facilityCode.ToUpper() + " " + facilityName.ToUpper(),
                                    StoreQuantity = facilityQty
                                });
                        }
                    }

                    Price[] listOfPrice = pricingDetail.ListOfPrice;
                    UnitPrice unitPrice = listOfPrice[0].UnitPrice;
                    materialDetail.MaterialPrice = unitPrice.UnitPriceValue;

                    if (!keyValuePairs.ContainsKey(materialDetail.ItemPositionNo))
                        keyValuePairs.Add(materialDetail.ItemPositionNo, materialDetail);
                    else
                    {
                        // manejar línea duplicada
                    }
                }
                else if (detailResponseCode == DetailResponseCode.ItemRejected)
                {
                    OriginalItemDetail originalItemDetail = orderResponseItemDetail.OriginalItemDetail;
                    PartNumbers partNumbers = originalItemDetail.ItemDetail.BaseItemDetail.ItemIdentifiers.PartNumbers;

                    materialDetail.MaterialNo = partNumbers.BuyerPartNumber.PartNum.PartID;
                    materialDetail.ItemPositionNo = originalItemDetail.ItemDetail.BaseItemDetail.LineItemNum.BuyerLineItemNum;
                    materialDetail.VendorMaterialNo = partNumbers.ManufacturerPartNumber.PartID;
                    materialDetail.VendorMaterialSos = partNumbers.ManufacturerPartNumber.PartIDExt;
                    materialDetail.Quantity = originalItemDetail.ItemDetail.BaseItemDetail.TotalQuantity.Quantity.QuantityValue.Value;
                    materialDetail.QuantityBackOrder = 0;
                    materialDetail.QuantityOnSite = 0;
                    materialDetail.IsAvailable = false;
                    materialDetail.HasReplacement = false;
                    materialDetail.MaterialResponseCode = MaterialResponseCode.ItemRejected;

                    string message = "";
                    if (orderResponseItemDetail.ListOfErrorInfo.Length >= 1)
                    {
                        ErrorInfo errorInfo = orderResponseItemDetail.ListOfErrorInfo[0];
                        message = errorInfo.CompletionMsg.LanguageString.LangString;

                        if (message.Contains("Indirectly Replaced"))
                            materialDetail.MaterialResponseCode = MaterialResponseCode.IndirectlyReplaced;
                    }

                    materialDetail.MaterialResponseText = message;

                    if (!keyValuePairs.ContainsKey(materialDetail.ItemPositionNo))
                        keyValuePairs.Add(materialDetail.ItemPositionNo, materialDetail);
                    else
                    {
                        // manejar línea duplicada
                    }
                }
                else if (detailResponseCode == DetailResponseCode.ReplacementItem)
                {
                    ItemDetailChanges itemDetailChanges = orderResponseItemDetail.ItemDetailChanges;
                    ItemDetail itemDetail = itemDetailChanges.ItemDetail;
                    PricingDetail pricingDetail = itemDetail.PricingDetail;
                    Price[] listOfPrice = pricingDetail.ListOfPrice;
                    UnitPrice unitPrice = listOfPrice[0].UnitPrice;
                    BaseItemDetail baseItemDetail = itemDetail.BaseItemDetail;
                    decimal totalQuantity = baseItemDetail.TotalQuantity.Quantity.QuantityValue.Value;
                    decimal backOrderQuantity = baseItemDetail.MaxBackOrderQuantity.Quantity.QuantityValue.Value;

                    ItemIdentifiers itemIdentifiers = baseItemDetail.ItemIdentifiers;
                    ManufacturerPartNumber manufacturerPartNumber = itemIdentifiers.PartNumbers.ManufacturerPartNumber;

                    OriginalItemDetail originalItemDetail = orderResponseItemDetail.OriginalItemDetail;
                    PartNumbers partNumbers = originalItemDetail.ItemDetail.BaseItemDetail.ItemIdentifiers.PartNumbers;
                    ItemDetail oriItemDetail = originalItemDetail.ItemDetail;
                    BaseItemDetail oriBaseItemDetail = oriItemDetail.BaseItemDetail;


                    BaseItemDetailTotalQuantity originalTotalQuantity = oriBaseItemDetail.TotalQuantity;
                    BaseItemDetailTotalQuantity changeTotalQuantity = baseItemDetail.TotalQuantity;

                    decimal originalValue = originalTotalQuantity.Quantity.QuantityValue.Value;
                    decimal changeValue = changeTotalQuantity.Quantity.QuantityValue.Value;

                    decimal result = changeValue / originalValue;

                    materialDetail.ItemPositionNo = originalItemDetail.ItemDetail.BaseItemDetail.LineItemNum.BuyerLineItemNum;

                    if (keyValuePairs.ContainsKey(materialDetail.ItemPositionNo))
                    {
                        materialDetail = keyValuePairs[materialDetail.ItemPositionNo];
                        materialDetail.MaterialResponseCode = MaterialResponseCode.MultiReplacement;
                    }
                    else
                    {
                        materialDetail.MaterialNo = partNumbers.BuyerPartNumber.PartNum.PartID;
                        materialDetail.VendorMaterialNo = partNumbers.ManufacturerPartNumber.PartID;
                        materialDetail.VendorMaterialSos = partNumbers.ManufacturerPartNumber.PartIDExt;
                        materialDetail.Quantity = originalItemDetail.ItemDetail.BaseItemDetail.TotalQuantity.Quantity.QuantityValue.Value;
                        materialDetail.QuantityBackOrder = 0;
                        materialDetail.QuantityOnSite = 0;
                        materialDetail.IsAvailable = false;
                        materialDetail.HasReplacement = true;
                        materialDetail.MaterialResponseCode = MaterialResponseCode.SingleReplacement;
                        keyValuePairs.Add(materialDetail.ItemPositionNo, materialDetail);
                    }

                    // decimal onSiteQuantity = totalQuantity - backOrderQuantity;
                    //SMaterialModel material = _mediator.Send(new GetMaterialQuery
                    //{
                    //    VendorMaterialNo = manufacturerPartNumber.PartID.Replace("-", ""),
                    //    VendorMaterialSos = manufacturerPartNumber.PartIDExt
                    //}).Result;

                    decimal onSiteQuantity = 0;

                    List<StoreAvailabilityModel> storeAvailabilities = new List<StoreAvailabilityModel>();

                    storeAvailabilities.Add(
                        new StoreAvailabilityModel
                        {
                            StoreName = "2081 Stracon On-Site",
                            StoreQuantity = onSiteQuantity.ToString()
                        });

                    if (backOrderQuantity > 0)
                    {
                        NameValueSet[] listOfNameValueSet = itemDetail.ListOfNameValueSet;

                        foreach (NameValueSet nameValueSet in listOfNameValueSet)
                        {
                            NameValuePair[] listOfNameValuePair = nameValueSet.ListOfNameValuePair;

                            string facilityCode = listOfNameValuePair[0].Value;
                            string facilityName = listOfNameValuePair[1].Value;
                            string facilityQty = listOfNameValuePair[2].Value;

                            if (facilityName == null || facilityName == "")
                                facilityName = "-";

                            storeAvailabilities.Add(
                                new StoreAvailabilityModel
                                {
                                    StoreName = facilityCode.ToUpper() + " " + facilityName.ToUpper(),
                                    StoreQuantity = facilityQty
                                });
                        }
                    }

                    materialDetail.MaterialReplacements.Add(
                        new MaterialBasicModel
                        {
                            VendorMaterialNo = manufacturerPartNumber.PartID,
                            VendorMaterialSos = manufacturerPartNumber.PartIDExt,
                            MaterialPrice = unitPrice.UnitPriceValue,
                            TotalQuantity = Convert.ToInt32(totalQuantity),
                            BackOrderQuantity = Convert.ToInt32(backOrderQuantity),
                            OnSiteQuantity = Convert.ToInt32(onSiteQuantity),
                            IsAvailable = true,
                            StoreAvailabilities = storeAvailabilities
                        });
                }
                else if (detailResponseCode == DetailResponseCode.Other)
                {
                    String detailResponseCodeOther = orderResponseItemDetail.ItemDetailResponseCodedOther;

                    if (detailResponseCodeOther.Contains("AlternateItem"))
                    {
                        OriginalItemDetail originalItemDetail = orderResponseItemDetail.OriginalItemDetail;
                        materialDetail.ItemPositionNo = originalItemDetail.ItemDetail.BaseItemDetail.LineItemNum.BuyerLineItemNum;

                        if (keyValuePairs.ContainsKey(materialDetail.ItemPositionNo))
                        {
                            ItemDetailChanges itemDetailChanges = orderResponseItemDetail.ItemDetailChanges;
                            ItemDetail itemDetail = itemDetailChanges.ItemDetail;
                            PricingDetail pricingDetail = itemDetail.PricingDetail;
                            Price[] listOfPrice = pricingDetail.ListOfPrice;
                            UnitPrice unitPrice = listOfPrice[0].UnitPrice;
                            BaseItemDetail baseItemDetail = itemDetail.BaseItemDetail;
                            ItemIdentifiers itemIdentifiers = baseItemDetail.ItemIdentifiers;
                            ManufacturerPartNumber manufacturerPartNumber = itemIdentifiers.PartNumbers.ManufacturerPartNumber;
                            decimal totalQuantity = baseItemDetail.TotalQuantity.Quantity.QuantityValue.Value;
                            decimal backOrderQuantity = baseItemDetail.MaxBackOrderQuantity.Quantity.QuantityValue.Value;

                            ////decimal onSiteQuantity = totalQuantity - backOrderQuantity;
                            //SMaterialModel material = _mediator.Send(new GetMaterialQuery
                            //{
                            //    VendorMaterialNo = manufacturerPartNumber.PartID.Replace("-", ""),
                            //    VendorMaterialSos = manufacturerPartNumber.PartIDExt
                            //}).Result;

                            decimal onSiteQuantity = 0;

                            materialDetail = keyValuePairs[materialDetail.ItemPositionNo];
                            materialDetail.HasAlternateItems = true;

                            List<StoreAvailabilityModel> storeAvailabilities = new List<StoreAvailabilityModel>();

                            storeAvailabilities.Add(
                                new StoreAvailabilityModel
                                {
                                    StoreName = "2081 Stracon On-Site",
                                    StoreQuantity = onSiteQuantity.ToString()
                                });

                            if (backOrderQuantity > 0)
                            {
                                NameValueSet[] listOfNameValueSet = itemDetail.ListOfNameValueSet;

                                foreach (NameValueSet nameValueSet in listOfNameValueSet)
                                {
                                    NameValuePair[] listOfNameValuePair = nameValueSet.ListOfNameValuePair;

                                    string facilityCode = listOfNameValuePair[0].Value;
                                    string facilityName = listOfNameValuePair[1].Value;
                                    string facilityQty = listOfNameValuePair[2].Value;

                                    if (facilityName == null || facilityName == "")
                                        facilityName = "-";

                                    storeAvailabilities.Add(
                                        new StoreAvailabilityModel
                                        {
                                            StoreName = facilityCode.ToUpper() + " " + facilityName.ToUpper(),
                                            StoreQuantity = facilityQty
                                        });
                                }
                            }

                            materialDetail.AlternateItems.Add(
                                new MaterialBasicModel
                                {
                                    VendorMaterialNo = manufacturerPartNumber.PartID,
                                    VendorMaterialSos = manufacturerPartNumber.PartIDExt,
                                    MaterialPrice = unitPrice.UnitPriceValue,
                                    TotalQuantity = Convert.ToInt32(totalQuantity),
                                    BackOrderQuantity = Convert.ToInt32(backOrderQuantity),
                                    OnSiteQuantity = Convert.ToInt32(onSiteQuantity),
                                    IsAvailable = true,
                                    StoreAvailabilities = storeAvailabilities
                                });
                        }
                        else
                        {
                            // alternate sin item principal
                        }
                    }
                    else
                    {
                        // apareció otro mensaje
                    }
                }
            }

            return keyValuePairs.Values.ToList();
        }
    }
}
