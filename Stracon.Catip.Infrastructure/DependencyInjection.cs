﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Stracon.Catip.Application.Common.Interfaces;
using Stracon.Catip.Infrastructure.Persistence;
using Stracon.Catip.Infrastructure.Services;

namespace Stracon.Catip.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(
                options => options.UseLazyLoadingProxies().UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)
                )
            );
            services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());
            services.AddTransient<IPriceAvailabilityService, PriceAvailabilityService>();
            services.AddTransient<IDirectOrderService, DirectOrderService>();
            return services;
        }
    }
}
