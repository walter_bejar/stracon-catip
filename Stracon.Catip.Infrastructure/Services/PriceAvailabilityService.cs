﻿using MediatR;
using Microsoft.Extensions.Logging;
using Stracon.Catip.Application.Common.Exceptions;
using Stracon.Catip.Application.Common.Interfaces;
using Stracon.Catip.Application.PriceAvailability.Query.GetAvailability;
using Stracon.Catip.Application.Configs.Queries.GetConfig;
using Stracon.Catip.Infrastructure.Models.Order;
using Stracon.Catip.Infrastructure.Models.OrderAcknowledgement;
using Stracon.Catip.Infrastructure.Models.OrderResponse;
using Stracon.Catip.Infrastructure.Utils.Request;
using Stracon.Catip.Infrastructure.Utils.Response;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Stracon.Catip.Infrastructure.Services
{
    public class PriceAvailabilityService : IPriceAvailabilityService
    {
        private readonly ILogger<PriceAvailabilityService> _logger;

        private readonly IMediator _mediator;

        private Dictionary<string, string> configs;

        private string user;
        private string pass;
        private string url;

        public PriceAvailabilityService(ILogger<PriceAvailabilityService> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
            configs = new Dictionary<string, string>();
        }

        private void LoadVariables()
        {
            user = _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "user"
            }).Result.ConfigVal;

            pass = _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "pass"
            }).Result.ConfigVal;

            url = _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "pa_url"
            }).Result.ConfigVal;

            configs.Add("account", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "account"
            }).Result.ConfigVal);

            configs.Add("supplier_id", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "supplier_id"
            }).Result.ConfigVal);

            configs.Add("customer_id", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "customer_id"
            }).Result.ConfigVal);

            configs.Add("store", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "store"
            }).Result.ConfigVal);

            configs.Add("refnum", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "refnum"
            }).Result.ConfigVal);

            configs.Add("transport_id", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "transport_id"
            }).Result.ConfigVal);

            configs.Add("shipping_instructions", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "shipping_instructions"
            }).Result.ConfigVal);

            configs.Add("dealer_code", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "dealer_code"
            }).Result.ConfigVal);
        }

        public async Task<List<MaterialAvailabilityModel>> CheckMaterialsAvailabilityAsync(List<MaterialModel> materials)
        {
            // Cargamos las variables de configuración
            LoadVariables();

            // Llamamos al servicio de forma normal y obtenemos su resultado
            return await CallPriceAvailabilityService(materials);
        }

        private async Task<List<MaterialAvailabilityModel>> CallPriceAvailabilityService(List<MaterialModel> materials)
        {
            using (var request = new HttpRequestMessage())
            {
                Order order = PriceAvailabilityRequest.CreateRequest(materials, configs, _mediator);

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Order));
                XmlSerializerNamespaces xmlSerializerNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                XmlWriterSettings WriterSettings = new XmlWriterSettings { OmitXmlDeclaration = false, Indent = true, Encoding = System.Text.Encoding.UTF8 };

                HttpClient client = new HttpClient();
                client.Timeout = TimeSpan.FromMinutes(10);

                using (var ms = new MemoryStream())
                using (var writer = XmlWriter.Create(ms, WriterSettings))
                {
                    xmlSerializer.Serialize(writer, order, xmlSerializerNamespaces);
                    string orderString = System.Text.Encoding.UTF8.GetString(ms.ToArray());

                    bool found = orderString.Contains(System.Text.Encoding.UTF8.GetString(System.Text.Encoding.UTF8.GetPreamble()));
                    if (found)
                        orderString = orderString.Remove(0, System.Text.Encoding.UTF8.GetString(System.Text.Encoding.UTF8.GetPreamble()).Length);

                    request.Content = new StringContent(orderString, System.Text.Encoding.UTF8);
                    request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/xml");
                    request.Content.Headers.ContentEncoding.Add("UTF-8");

                    string auth = user + ":" + pass; //"S4HudbayQA01:Ferreyros20@"; //"DBSYURAQA01:Ferreyros20@";
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(auth);
                    string encode64 = Convert.ToBase64String(bytes);
                    request.Headers.Authorization = AuthenticationHeaderValue.Parse("Basic " + encode64);
                    request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                    request.Headers.AcceptEncoding.Add(new StringWithQualityHeaderValue("UTF-8"));

                    request.Method = new HttpMethod("POST");
                    //request.RequestUri = new Uri("https://qapartsb2bwsx.rd.cat.com/psb2b/ReceiveDocumentServlet/xCBL30_BASIC_SyncInq");
                    request.RequestUri = new Uri(url);

                    Stopwatch timer = new Stopwatch();
                    timer.Start();

                    var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);
                    var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                    timer.Stop();
                    var elapsedMilliseconds = timer.ElapsedMilliseconds;

                    var requestName = typeof(PriceAvailabilityService).Name;
                    _logger.LogInformation("Catip PriceAvailability - Total time running request: {Name} ({ElapsedMilliseconds} milliseconds) {@Request}",
                    requestName, elapsedMilliseconds, request);

                    if (response.IsSuccessStatusCode)
                    {
                        try
                        {
                            XmlSerializer xmlDeserializer = new XmlSerializer(typeof(OrderResponse));
                            OrderResponse orderResponse = (OrderResponse)xmlDeserializer.Deserialize(new StringReader(responseText));
                            List<MaterialAvailabilityModel> materialAvailabilities = PriceAvailabilityResponse.ReadResponse(orderResponse, _mediator);
                            return materialAvailabilities;
                        }
                        catch (Exception e)
                        {
                            if (e is InvalidOperationException)
                            {
                                XmlSerializer xmlDeserializer = new XmlSerializer(typeof(OrderAcknowledgement));
                                OrderAcknowledgement orderAcknowledgement = (OrderAcknowledgement)xmlDeserializer.Deserialize(new StringReader(responseText));
                                throw new CatipException(orderAcknowledgement.DetailMessage);
                            }
                            throw e;
                        }
                    }
                }
            }

            return new List<MaterialAvailabilityModel>();
        }
    }
}
