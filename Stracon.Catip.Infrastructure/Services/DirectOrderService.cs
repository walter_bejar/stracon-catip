﻿using MediatR;
using Microsoft.Extensions.Logging;
using Stracon.Catip.Application.Common.Exceptions;
using Stracon.Catip.Application.Common.Interfaces;
using Stracon.Catip.Application.Configs.Queries.GetConfig;
using Stracon.Catip.Application.DirectOrder.Commands.PostOrder;
using Stracon.Catip.Infrastructure.Models.Order;
using Stracon.Catip.Infrastructure.Models.OrderAcknowledgement;
using Stracon.Catip.Infrastructure.Utils.Request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Stracon.Catip.Infrastructure.Services
{
    public class DirectOrderService : IDirectOrderService
    {
        private readonly ILogger<DirectOrderService> _logger;

        private readonly IMediator _mediator;

        private string user;
        private string pass;
        private string url;

        private Dictionary<string, string> configs;

        public DirectOrderService(ILogger<DirectOrderService> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
            configs = new Dictionary<string, string>();
        }

        private void LoadVariables()
        {
            user = _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "user"
            }).Result.ConfigVal;

            pass = _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "pass"
            }).Result.ConfigVal;

            url = _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "do_url"
            }).Result.ConfigVal;

            configs.Add("account", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "account"
            }).Result.ConfigVal);

            configs.Add("supplier_id", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "supplier_id"
            }).Result.ConfigVal);

            configs.Add("customer_id", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "customer_id"
            }).Result.ConfigVal);

            configs.Add("store", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "store"
            }).Result.ConfigVal);

            configs.Add("refnum", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "refnum"
            }).Result.ConfigVal);

            configs.Add("transport_id", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "transport_id"
            }).Result.ConfigVal);

            configs.Add("shipping_instructions", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "shipping_instructions"
            }).Result.ConfigVal);

            configs.Add("dealer_code", _mediator.Send(new GetConfigQuery
            {
                ConfigKey = "dealer_code"
            }).Result.ConfigVal);
        }

        public async Task PlaceOrderAsync(OrderModel materialsOrder)
        {
            // Cargamos las variables de configuración
            LoadVariables();

            using (var request = new HttpRequestMessage())
            {
                Order order = DirectOrderRequest.CreateRequest(materialsOrder, configs, _mediator);

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Order));
                XmlSerializerNamespaces xmlSerializerNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                XmlWriterSettings WriterSettings = new XmlWriterSettings { OmitXmlDeclaration = false, Indent = true, Encoding = System.Text.Encoding.UTF8 };

                HttpClient client = new HttpClient();

                using (var ms = new MemoryStream())
                using (var writer = XmlWriter.Create(ms, WriterSettings))
                {
                    xmlSerializer.Serialize(writer, order, xmlSerializerNamespaces);
                    string orderString = System.Text.Encoding.UTF8.GetString(ms.ToArray());

                    bool found = orderString.Contains(System.Text.Encoding.UTF8.GetString(System.Text.Encoding.UTF8.GetPreamble()));
                    if (found)
                        orderString = orderString.Remove(0, System.Text.Encoding.UTF8.GetString(System.Text.Encoding.UTF8.GetPreamble()).Length);

                    request.Content = new StringContent(orderString, System.Text.Encoding.UTF8);
                    request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/xml");
                    request.Content.Headers.ContentEncoding.Add("UTF-8");

                    string auth = user + ":" + pass; //"S4HudbayQA01:Ferreyros20@"; //"DBSYURAQA01:Ferreyros20@";
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(auth);
                    string encode64 = System.Convert.ToBase64String(bytes);
                    request.Headers.Authorization = AuthenticationHeaderValue.Parse("Basic " + encode64);
                    request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                    request.Headers.AcceptEncoding.Add(new StringWithQualityHeaderValue("UTF-8"));

                    request.Method = new HttpMethod("POST");
                    //request.RequestUri = new Uri("https://qapartsb2bwsx.rd.cat.com/psb2b/ReceiveDocumentServlet/xCBL30_BASIC_AsyncInq");
                    request.RequestUri = new Uri(url);

                    var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);
                    var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                    if (response.IsSuccessStatusCode)
                    {
                        try
                        {
                            XmlSerializer xmlDeserializer = new XmlSerializer(typeof(OrderAcknowledgement));
                            OrderAcknowledgement orderAcknowledgement = (OrderAcknowledgement)xmlDeserializer.Deserialize(new StringReader(responseText));

                            if (!orderAcknowledgement.Status.Contains("Success"))
                                throw new CatipException(orderAcknowledgement.DetailMessage);
                        }
                        catch (Exception e)
                        {
                            throw e;
                        }
                    }
                }
            }
        }
    }
}
